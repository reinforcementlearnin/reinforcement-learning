import gym
import numpy as np
import time, pickle, os

env = gym.make('FrozenLake-v0')

total_episodes = 10
max_steps = 100

with open("frozenLake_qTable_sarsa.pkl", 'rb') as f:
	Q = pickle.load(f)

def choose_action(state):
	action = 0
	action = np.argmax(Q[state, :])
	return action

if __name__ == '__main__':
	win = 0
	lose = 0
	for episode in range(total_episodes):
		t = 0
		state = env.reset()
		action = choose_action(state)
	
		while t < max_steps:
			os.system('cls')
			env.render()
	
			state2, reward, done, info = env.step(action)
			action2 = choose_action(state2)
	
			state = state2
			action = action2
	
			t += 1
	
			if done:
				if reward > 0:
					print('WIN')
					win += 1
					time.sleep(1)
				else:
					print('LOSE')
					lose += 1
					time.sleep(0.5)
				os.system('cls')
				break
	
	print("Win/lose: ", win, "/", lose)
	input()