from __future__ import print_function, division
from builtins import range

import copy
import gym
import os
import sys
import random
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from gym import wrappers
from datetime import datetime
from scipy.misc import imresize
import Constants
from ImageTransformer import ImageTransformer
from ReplayMemory import ReplayMemory
from DQN import DQN

def update_state(state, obs_small):
    return np.append(state[:, :, 1:], np.expand_dims(obs_small, 2), axis=2)

def learn(model, target_model, experience_replay_buffer, gamma, batch_size):
    # Sample experiences
    states, actions, rewards, next_states, dones = experience_replay_buffer.get_minibatch()

    # Calculate targets
    next_Qs = target_model.predict(next_states)
    next_Q = np.amax(next_Qs, axis=1)
    targets = rewards + np.invert(dones).astype(np.float32) * gamma * next_Q

    # Update model
    loss = model.update(states, actions, targets)
    return loss


def play_one(
        env,
        sess,
        total_t,
        experience_replay_buffer,
        model,
        target_model,
        image_transformer,
        gamma,
        batch_size,
        epsilon,
        epsilon_change,
        epsilon_min):
    t0 = datetime.now()

    # Reset the environment
    obs = env.reset()
    obs_small = image_transformer.transform(obs, sess)
    state = np.stack([obs_small] * 4, axis=2)
    loss = None

    total_time_training = 0
    num_steps_in_episode = 0
    episode_reward = 0

    done = False
    while not done:
        #env.render()
        # Update target network
        if total_t % Constants.TARGET_UPDATE_PERIOD == 0:
            target_model.copy_from(model)
            print("Copied model parameters to target network. total_t = %s, period = %s" % (
            total_t, Constants.TARGET_UPDATE_PERIOD))

        # Take action
        action = model.sample_action(state, epsilon)
        obs, reward, done, _ = env.step(action)
        obs_small = image_transformer.transform(obs, sess)
        next_state = update_state(state, obs_small)

        # Compute total reward
        episode_reward += reward

        # Save the latest experience
        experience_replay_buffer.add_experience(action, obs_small, reward, done)

        # Train the model, keep track of time
        t0_2 = datetime.now()
        loss = learn(model, target_model, experience_replay_buffer, gamma, batch_size)
        dt = datetime.now() - t0_2

        # More debugging info
        total_time_training += dt.total_seconds()
        num_steps_in_episode += 1

        state = next_state
        total_t += 1

        epsilon = max(epsilon - epsilon_change, epsilon_min)

    return total_t, episode_reward, (
                datetime.now() - t0), num_steps_in_episode, total_time_training / num_steps_in_episode, epsilon


def smooth(x):
    # last 100
    n = len(x)
    y = np.zeros(n)
    for i in range(n):
        start = max(0, i - 99)
        y[i] = float(x[start:(i + 1)].sum()) / (i - start + 1)
    return y


if __name__ == '__main__':

    # hyperparams and initialize stuff
    conv_layer_sizes = [(32, 8, 4), (64, 4, 2), (64, 3, 1)]
    hidden_layer_sizes = [512]
    gamma = 0.99
    batch_sz = 32
    num_episodes = 5

    total_t = 0
    experience_replay_buffer = ReplayMemory()
    episode_rewards = np.zeros(num_episodes)

    # epsilon
    # decays linearly until 0.1
    epsilon = 1
    epsilon_min = 0.1
    epsilon_change = (epsilon - epsilon_min) / 500000

    # Create environment
    env = gym.envs.make("Breakout-v0")

    # Create models
    model = DQN(
        K=Constants.K,
        conv_layer_sizes=conv_layer_sizes,
        hidden_layer_sizes=hidden_layer_sizes,
        scope="model")
    target_model = DQN(
        K=Constants.K,
        conv_layer_sizes=conv_layer_sizes,
        hidden_layer_sizes=hidden_layer_sizes,
        scope="target_model"
    )
    image_transformer = ImageTransformer()

    with tf.Session() as sess:
        model.set_session(sess)
        target_model.set_session(sess)
        sess.run(tf.global_variables_initializer())

        print("Populating experience replay buffer...")
        obs = env.reset()

        for i in range(Constants.MIN_EXPERIENCES):

            action = np.random.choice(Constants.K)
            obs, reward, done, _ = env.step(action)
            obs_small = image_transformer.transform(obs, sess)  # not used anymore
            experience_replay_buffer.add_experience(action, obs_small, reward, done)
            if done:
                obs = env.reset()

        # Play a number of episodes and learn!
        t0 = datetime.now()
        for i in range(num_episodes):
            total_t, episode_reward, duration, num_steps_in_episode, time_per_step, epsilon = play_one(
                env,
                sess,
                total_t,
                experience_replay_buffer,
                model,
                target_model,
                image_transformer,
                gamma,
                batch_sz,
                epsilon,
                epsilon_change,
                epsilon_min,
            )
            episode_rewards[i] = episode_reward

            last_100_avg = episode_rewards[max(0, i - 100):i + 1].mean()
            print("Episode:", i,
                  "Duration:", duration,
                  "Num steps:", num_steps_in_episode,
                  "Reward:", episode_reward,
                  "Training time per step:", "%.3f" % time_per_step,
                  "Avg Reward (Last 100):", "%.3f" % last_100_avg,
                  "Epsilon:", "%.3f" % epsilon
                  )
            sys.stdout.flush()
        print("Total duration:", datetime.now() - t0)

        model.save()

        # Plot the smoothed returns
        y = smooth(episode_rewards)
        plt.plot(episode_rewards, label='orig')
        plt.plot(y, label='smoothed')
        plt.legend()
        plt.show()